// soal1
console.log('LOOPING PERTAMA')

let angka1 = 2;

while (angka1 <= 20) {
    console.log(`${angka1} - I love coding`);
    angka1 += 2;
}

console.log('LOOPING KEDUA')

let angka2 = 20

while (angka2 > 0) {
    console.log(`${angka2} - I will become a mobile developer`);
    angka2 -= 2;
}

// soal2
console.log(`-------------------------------------------------------------------`)

for (let i = 1; i < 21; i++) {
    if (i % 3 == 0 && i % 2 !== 0) {
        console.log(`${i} - I Love Coding`)
    }
    else if (i % 2 == 0) {
        console.log(`${i} - Berkualitas`)
    }
    else {
        console.log(`${i} - Santai`)
    }
}

// soal3
console.log(`-------------------------------------------------------------------`)

let pagarVertikal = 4
let pagarHorizontal = 8
let hasil3 = ''

for (let i = 0; i < pagarVertikal; i++) {
    for (let j = 0; j < pagarHorizontal; j++) {
        hasil3 += '#'
    }
    hasil3 += '\n'
}
console.log(hasil3)

// soal4
console.log(`-------------------------------------------------------------------`)

let piramid = 7
let hasil4 = ''

for (let k = 0; k < piramid; k++) {
    for (let l = 0; l <= k; l++) {
        hasil4 += '#'
    }
    hasil4 += '\n'
}
console.log(hasil4)

// soal5
console.log(`-------------------------------------------------------------------`)

let catur = 8
let hasil5 = ''
for (let m = 0; m < catur; m++) {
    for (let n = 0; n < catur; n++) {
        if (m % 2 == 0 && n % 2 == 0) {
            hasil5 += ' '
        } else if (m % 2 !== 0 && n % 2 !== 0) {
            hasil5 += ' '
        } else {
            hasil5 += '#'
        }
    }
    hasil5 += '\n'
}
console.log(hasil5)
